/*
 * Any copyright is dedicated to the Public Domain - NO WARRANTY:
 *    <http://creativecommons.org/publicdomain/zero/1.0/>
 *
 * Written by Alfonso Sabato Siciliano
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>
#include <sysctlmibinfo.h>

/*
 * Traverse the MIB, build sysctlmif_objects and print the properties.
 */
int main()
{
	struct sysctlmif_object *obj;
	int id[SYSCTLMIF_MAXIDLEVEL], idnext[SYSCTLMIF_MAXIDLEVEL];
	size_t idlevel, idnextlevel, i;

	id[0] = 0;
	idlevel = 1;

	for (;;) {
		if ((obj = sysctlmif_object(id, idlevel, SYSCTLMIF_FALL)) == NULL)
			return (1);

		printf("sysctlmif_object_t *obj {\n");
		printf("\t obj->id:      ");
		for (i = 0; i < idlevel; i++)
			printf("%x%c", obj->id[i], i+1 < obj->idlevel ? '.' : '\n');
		printf("\t obj->idlevel: %zu\n", obj->idlevel);
		printf("\t obj->name:    %s\n", obj->name);
		printf("\t obj->desc:    %s\n", obj->desc  ? obj->desc  : "");
		printf("\t obj->label:   %s\n", obj->label ? obj->label : "");
		printf("\t obj->type:    %u\n", obj->type);
		printf("\t obj->flags:   %x\n", obj->flags);
		printf("\t obj->fmt:     %s\n", obj->fmt);
		printf("};\n");

		sysctlmif_freeobject(obj);

		printf("-------------------------------------------\n");

		idnextlevel = SYSCTLMIF_MAXIDLEVEL;
		/* if (sysctlmif_nextnode(id,idlevel,idnext,&idnextlevel)<0) */
		if (sysctlmif_nextleaf(id, idlevel, idnext, &idnextlevel) < 0)
			break;

		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;
	}

	return (0);
}

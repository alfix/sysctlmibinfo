/*
 * Any copyright is dedicated to the Public Domain - NO WARRANTY:
 *    <http://creativecommons.org/publicdomain/zero/1.0/>
 *
 * Written by Alfonso Sabato Siciliano
 */

#include <sys/types.h>
#include <sys/queue.h>

#include <stdio.h>
#include <sysctlmibinfo.h>

/* preorder visit */
void show_tree(struct sysctlmif_object *node)
{
	struct sysctlmif_object *child;

	printf("%s\n", node->name);

	if (node->children == NULL)
		return;

	SLIST_FOREACH(child, node->children, object_link)
		show_tree(child);
}

/*
 * The complete sysctl MIB-Tree
 */
int main()
{
	struct sysctlmif_list *mib;
	struct sysctlmif_object *root;

	printf("---------- MIB ----------\n");
	if ((mib = sysctlmif_mib(SYSCTLMIF_FNAME)) == NULL)
		return (1);

	SLIST_FOREACH(root, mib, object_link)
		show_tree(root);

	sysctlmif_freemib(mib);

	return (0);
}

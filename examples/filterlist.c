/*
 * Any copyright is dedicated to the Public Domain - NO WARRANTY:
 *    <http://creativecommons.org/publicdomain/zero/1.0/>
 *
 * Written by Alfonso Sabato Siciliano
 */

#include <sys/types.h>
#include <sys/queue.h>

#include <stdio.h>
#include <sysctlmibinfo.h>

/* filter top-level objects */
static int filter(struct sysctlmif_object *obj)
{
	return (obj->idlevel == 1 ? 0 : -1);
}

/*
 * sysctlmif_filterlist() example.
 */
int main()
{
	struct sysctlmif_list *list;
	struct sysctlmif_object *obj;

	printf("\n\n---------- sysctlmif_filterlist() ----------\n");
	if ((list = sysctlmif_filterlist(filter, SYSCTLMIF_FNAME)) == NULL)
		return (1);

	SLIST_FOREACH(obj, list, object_link)
		printf("%s\n", obj->name);

	sysctlmif_freelist(list);

	return (0);
}

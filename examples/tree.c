/*
 * Any copyright is dedicated to the Public Domain - NO WARRANTY:
 *    <http://creativecommons.org/publicdomain/zero/1.0/>
 *
 * Written by Alfonso Sabato Siciliano
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/sysctl.h>

#include <string.h>
#include <stdio.h>
#include <sysctlmibinfo.h>

/* Preorder visit */
void show_tree(struct sysctlmif_object *node)
{
	struct sysctlmif_object *child;

	printf("%s\n", node->name);

	if (node->children == NULL)
		return;

	SLIST_FOREACH(child, node->children, object_link)
		show_tree(child);
}

/*
 * Sub-tree examples
 */
int main()
{
	struct sysctlmif_object *root;;
	int id[SYSCTLMIF_MAXIDLEVEL];
	size_t idlevel;

	idlevel = SYSCTLMIF_MAXIDLEVEL;
	if (sysctlmif_nametoid("vm", strlen("vm"), id, &idlevel) != 0)
		return (1);

	/* "vm" max depth 2 edges */
	printf("\n--- vm.*.* --- \n");
	if ((root = sysctlmif_tree(id, idlevel, SYSCTLMIF_FNAME, 2)) == NULL)
		return (1);

	show_tree(root);
	sysctlmif_freetree(root);

	/* "vm" depth 0 edge */
	printf("\n--- only vm --- \n");
	if ((root = sysctlmif_tree(id, idlevel, SYSCTLMIF_FNAME, 0)) == NULL)
		return (1);

	show_tree(root);
	sysctlmif_freetree(root);

	return (0);
}

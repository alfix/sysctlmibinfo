/*
 * Any copyright is dedicated to the Public Domain - NO WARRANTY:
 *    <http://creativecommons.org/publicdomain/zero/1.0/>
 *
 * Written by Alfonso Sabato Siciliano
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>
#include <sysctlmibinfo.h>

#define BUFSIZE    1024

/*
 * Traverse the MIB-Tree and print the properties of each object.
 */
int main()
{
	int id[SYSCTLMIF_MAXIDLEVEL], idnext[SYSCTLMIF_MAXIDLEVEL];
	int name2id[SYSCTLMIF_MAXIDLEVEL];
	char buf[BUFSIZE];
	size_t idlevel, idnextlevel, buflen, name2idlevel, i;

	id[0] = 0;
	idlevel = 1;

	for (;;) {
		printf("id: ");
		for (i = 0; i < idlevel; i++) {
			printf("%x%c", id[i], i+1 < idlevel ? '.' : '\n');
		}

		/* NAMELEN and name */
		buflen = BUFSIZE;
		if (SYSCTLMIF_NAMELEN(id, idlevel, &buflen) != 0)
			return (1);

		printf("name [%zuB]: ", buflen);
		if (sysctlmif_name(id, idlevel, buf, &buflen) != 0)
			return (1);

		printf("%s\n", buf);

		/* nametoid */
		name2idlevel = SYSCTLMIF_MAXIDLEVEL;
		if (sysctlmif_nametoid(buf, buflen, name2id, &name2idlevel) != 0)
			return (1); 

		printf("nametoid: ");
		for (i = 0; i < name2idlevel; i++)
			printf("%x%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');

		/* DESCLEN and desc */
		buflen = BUFSIZE;
		if (SYSCTLMIF_DESCLEN(id, idlevel, &buflen) != 0)
			buflen = 0;

		printf("descr [%zuB]: ", buflen);
		if (sysctlmif_desc(id, idlevel, buf, &buflen) != 0)
			memset((void *)buf, 0, BUFSIZE);

		printf("%s\n", buf);

		/* LABELLEN and label */
		buflen = BUFSIZE;
		if (SYSCTLMIF_LABELLEN(id, idlevel, &buflen) != 0)
			buflen = 0;

		printf("label [%zuB]: ", buflen);
		if (sysctlmif_label(id, idlevel, buf, &buflen) != 0)
			memset((void *)buf, 0, BUFSIZE);

		printf("%s\n", buf);

		/* info: INFOKIND, INFOTYPE, INFOFLAGS, INFOFMT */
		buflen = BUFSIZE;
		printf("info: \n");
		if (sysctlmif_info(id, idlevel, (void *)buf, &buflen) != 0)
			return (1);

		printf("   INFOKIND(info): %x\n", SYSCTLMIF_INFOKIND(buf));
		printf("   INFOFLAGS(info): %x\n", SYSCTLMIF_INFOFLAGS(buf));
		printf("   INFOTYPE(info): %u\n", SYSCTLMIF_INFOTYPE(buf));
		printf("   INFOFMT(info): %s\n", SYSCTLMIF_INFOFMT(buf));

		printf("-------------------------------------------\n");

		/* nextleaf or nextnode */
		idnextlevel = SYSCTLMIF_MAXIDLEVEL;
		/* if(sysctlmif_nextnode(id,idlevel,idnext,&idnextlevel)!=0) */
		if(sysctlmif_nextleaf(id, idlevel, idnext, &idnextlevel) != 0)
			break;

		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;
	}

	return (0);
}

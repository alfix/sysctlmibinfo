/*
 * Any copyright is dedicated to the Public Domain - NO WARRANTY:
 *    <http://creativecommons.org/publicdomain/zero/1.0/>
 *
 * Written by Alfonso Sabato Siciliano
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/sysctl.h>

#include <string.h>
#include <stdio.h>
#include <sysctlmibinfo.h>

/*
 * sysctlmif_grouplist()
 */
int main()
{
	int id[SYSCTLMIF_MAXIDLEVEL];
	size_t level = SYSCTLMIF_MAXIDLEVEL;
	struct sysctlmif_list *list;
	struct sysctlmif_object *obj;

	printf("\n\n---------- sysctlmif_grouplist() ----------\n");
	if (sysctlmif_nametoid("vm", strlen("vm") + 1, id, &level) != 0)
		return (1);

	/* "vm" max depth 2 level */
	printf("\n--- vm.*.* --- \n");
	if ((list = sysctlmif_grouplist(id, level, SYSCTLMIF_FNAME, 2)) == NULL)
		return (1);

	SLIST_FOREACH(obj, list, object_link)
		printf("%s\n", obj->name);

	sysctlmif_freelist(list);

	/* "vm" depth 0 */
	printf("\n\n--- only vm --- \n");
	if ((list = sysctlmif_grouplist(id, level, SYSCTLMIF_FNAME, 0)) == NULL)
		return (1);

	SLIST_FOREACH(obj, list, object_link)
		printf("%s\n", obj->name);

	sysctlmif_freelist(list);

	return (0);
}

/*
 * Any copyright is dedicated to the Public Domain - NO WARRANTY:
 *    <http://creativecommons.org/publicdomain/zero/1.0/>
 *
 * Written by Alfonso Sabato Siciliano
 */

#include <sys/types.h>
#include <sys/queue.h>

#include <stdio.h>
#include <sysctlmibinfo.h>

/*
 * SYSCTLMIF_LIST(flags),
 * alias for: sysctlmif_filterlist(NULL, flags).
 */
int main()
{
	struct sysctlmif_list *list;
	struct sysctlmif_object *obj;
	
	printf("---------- SYSCTLMIF_LIST() ----------\n\n");
	if ((list = SYSCTLMIF_LIST(SYSCTLMIF_FNAME)) == NULL)
		return (1);

	SLIST_FOREACH(obj, list, object_link)
		printf("%s\n", obj->name);

	sysctlmif_freelist(list);

	return (0);
}

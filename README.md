sysctlmibinfo 1.0.2
===================

 1. [Introduction](#introduction)
 2. [Getting started](#getting-started)
 3. [API](#api)
 4. [Manual](#manual)
 5. [Examples](#examples)
 6. [Caveats](#caveats)
 7. [Off-Topic](#off-topic)
 8. [Extras](#extras)


Introduction
--------------

The sysctlmibinfo library provides an API to explore the
[FreeBSD](https://www.freebsd.org) sysctl MIB and to get the info of an object,
therefore it is useful to handle an object correctly and/or to build a
[sysctl](https://www.freebsd.org/doc/handbook/configtuning-sysctl.html)-like
utility.

You may want to use the new version
[sysctlmibinfo2](https://gitlab.com/alfix/sysctlmibinfo2),
it provides an improved and more efficient API on a new kernel interface.
This version will continue to live because some developer/user has installed it,
but updates and improvements will be sporadic.

Getting started
-----------------

To install the port
[<devel/libsysctlmibinfo>](https://www.freshports.org/devel/libsysctlmibinfo/):

	# cd /usr/ports/devel/libsysctlmibinfo/ && make install clean

To add the package:

	# pkg install libsysctlmibinfo


To compile a project with sysctlmibinfo:

```c
#include <sys/types.h>
#include <sys/queue.h>
#include <sys/sysctl.h>

#include <sysctlmibinfo.h>

int main() {
	/* sysctlmibinfo API */
	return (0);
}
```
```
% cc -I/usr/local/include myproject.c -L/usr/local/lib -lsysctlmibinfo -o myproject
```

API
---

Primarily sysctlmibinfo wraps the kernel interface to provide an easy C API
to explore the MIB-Tree for getting the info of an object.
```c
/* object info */
int sysctlmif_nametoid(const char *name, size_t namelen, int *id, size_t *idlevel);
int sysctlmif_name(int *id, size_t idlevel, char *name, size_t *namelen);
int SYSCTLMIF_NAMELEN(int *id, size_t idlevel, size_t *namelen);
int sysctlmif_desc(int *id, size_t idlevel, char *desc, size_t *desclen);
int SYSCTLMIF_DESCLEN(int *id, size_t idlevel, size_t *desclen);
int sysctlmif_label(int *id, size_t idlevel, char *label, size_t *labellen);
int SYSCTLMIF_LABELLEN(int *id, size_t idlevel, size_t *labellen);
int sysctlmif_info(int *id, size_t idlevel, void *info, size_t *infolen);
uint32_t SYSCTLMIF_INFOKIND(info);
uint8_t SYSCTLMIF_INFOTYPE(info);
uint32_t SYSCTLMIF_INFOFLAGS(info);
char *SYSCTLMIF_INFOFMT(info);
/* explore MIB-Tree */
int sysctlmif_nextnode(int *id, size_t idlevel, int *idnext, size_t *idnextlevel);
int sysctlmif_nextleaf(int *id, size_t idlevel, int *idnext, size_t *idnextlevel);
```

Moreover sysctlmibinfo provides a high level API: a *struct sysctlmif\_object*
definition and functions to build data structures of objects.
```c
SLIST_HEAD(sysctlmif_list, sysctlmif_object);

struct sysctlmif_object {
	SLIST_ENTRY(sysctlmif_object) object_link;
	int      *id;      /* array of idlevel entries           */
	size_t   idlevel;  /* between 1 and SYSCTLMIF_MAXIDLEVEL */
	char     *name;    /* name in MIB notation               */
	char     *desc;    /* description                        */
	char     *label;   /* aggregation label                  */
	uint8_t  type;     /* defined in <sys/sysctl.h>          */
	uint32_t flags;    /* defined in <sys/sysctl.h>          */
	char     *fmt;     /* format string                      */
	struct sysctlmif_list *children; /* children list        */
};

/*
 * OR FLAGS: object fields to set,
 * id and idlevel are always set,
 * children list is set by sysctlmif_tree() and sysctlmif_mib().
 */
#define SYSCTLMIF_FNAME         0x01    /* name  */
#define SYSCTLMIF_FDESC         0x02    /* desc  */
#define SYSCTLMIF_FLABEL        0x04    /* label */
#define SYSCTLMIF_FTYPE         0x08    /* type  */
#define SYSCTLMIF_FFLAGS        0x10    /* flags */
#define SYSCTLMIF_FFMT          0x20    /* fmt   */
#define SYSCTLMIF_FALL                  /* all   */

/* single object */
struct sysctlmif_object *
sysctlmif_object(int *id, size_t idlevel, unsigned int flags);
void sysctlmif_freeobject(struct sysctlmif_object *object);
/* list */
typedef int sysctlmif_filterfunc_t (struct sysctlmif_object *object);
struct sysctlmif_list *
sysctlmif_filterlist(sysctlmif_filterfunc_t *filterfunc, unsigned int flags);
struct sysctlmif_list *SYSCTLMIF_LIST(flags);
struct sysctlmif_list *
sysctlmif_grouplist(int *id, size_t idlevel, unsigned int flags, unsigned int max_depth);
void sysctlmif_freelist(struct sysctlmif_list *list);
/* tree */
struct sysctlmif_object *
sysctlmif_tree(int *id, size_t idlevel, unsigned int flags, unsigned int max_depth);
void sysctlmif_freetree(struct sysctlmif_object *object_root);
/* mib */
struct sysctlmif_list *sysctlmif_mib(unsigned int flags);
void sysctlmif_freemib(struct sysctlmif_list *mib);
```

Manual
------

[sysctlmibinfo(3)](https://alfonsosiciliano.gitlab.io/posts/2019-01-25-sysctlmibinfo.html).

Examples
--------

The [examples](https://gitlab.com/alfix/sysctlmibinfo/tree/master/examples) are
in the _Public Domain_ for getting started your projects:

 - [wrappers.c](examples/wrappers.c) - Useful for getting one property - *% sysctl -a*
 - [object.c](examples/object.c) - For getting all the properties - *% sysctl -a*
 - [list.c](examples/list.c) - All objects in a list - *% sysctl -aN*
 - [filterlist.c](examples/filterlist.c) - top-level objects
 - [grouplist.c](examples/grouplist.c) - Objects list of a Depth First Traversal
 - [tree.c](examples/tree.c) - Subtree of the MIB - _% sysctl -N vm.\*.\*_ 3 levels
 - [mib.c](examples/mib.c) - Complete MIB - *% sysctl -aN*

Caveats
-------

sysctlmibinfo is built on the current undocumented kernel interface so it
inherits its limitations, they are listed in the sections: CAVEATS and BUGS
of the manual
[sysctlmibinfo(3)](https://alfonsosiciliano.gitlab.io/posts/2019-01-25-sysctlmibinfo.html).

Off-Topic
---------

get/set an object value [sysctl(3)](https://man.freebsd.org/sysctl/3), example
for a sysctlmif\_object:
```c
sysctl(object->id, object->idlevel, oldp, oldplen, newp, newplen);
```

add/remove an object [sysctl(9)](https://man.freebsd.org/sysctl/9).

Extras
------

 * mailing list: http://lists.freebsd.org/pipermail/freebsd-hackers/2018-December/053767.html

/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2018-2021 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/sysctl.h>

#include <stdlib.h>
#include <string.h>

#include "sysctlmibinfo.h"

/* FreeBSD <= 13 */
#ifndef CTL_SYSCTL
#define CTL_SYSCTL              0
#define CTL_SYSCTL_NAME         1
#define CTL_SYSCTL_NEXT         2
#define CTL_SYSCTL_NAME2OID     3
#define CTL_SYSCTL_OIDFMT       4
#define CTL_SYSCTL_OIDDESCR     5
#define CTL_SYSCTL_OIDLABEL     6
#endif

int
sysctlmif_nametoid(const char *name, size_t namelen, int *id, size_t *idlevel)
{
	int error, mib[2] = {CTL_SYSCTL, CTL_SYSCTL_NAME2OID};

	*idlevel *= sizeof(int);
	error = sysctl(mib, 2, id, idlevel, name, namelen);
	*idlevel /= sizeof(int);

	return (error);
}

/*
 * WARNING
 * 'mib[CTL_MAXNAME + 2]': '+ 2' to avoid a memory fault but the kernel
 * returns an error because the max idlevel is CTL_MAXNAME (now 24),
 * so the undocumented interface cannot handle an OID over CTL_MAXNAME - 2
 * levels, that is error with 23 or 24 levels.
 *
 * Extra caveats and bugs from the undocumented interface are in the manual.
 */
int sysctlmif_desc(int *id, size_t idlevel, char *desc, size_t *desclen)
{
	int mib[CTL_MAXNAME + 2] = {CTL_SYSCTL, CTL_SYSCTL_OIDDESCR};

	memcpy(mib + 2, id, idlevel * sizeof(int));
	return (sysctl(mib, idlevel + 2, (void *)desc, desclen, NULL, 0));
}

int sysctlmif_name(int *id, size_t idlevel, char *name, size_t *namelen)
{
	int mib[CTL_MAXNAME + 2] = {CTL_SYSCTL, CTL_SYSCTL_NAME};

	memcpy(mib + 2, id, idlevel * sizeof(int));
	return (sysctl(mib, idlevel + 2, (void *)name, namelen, NULL, 0));
}

int sysctlmif_label(int *id, size_t idlevel, char *label, size_t *labellen)
{
	int mib[CTL_MAXNAME + 2] = {CTL_SYSCTL, CTL_SYSCTL_OIDLABEL};

	memcpy(mib + 2, id, idlevel * sizeof(int));
	return (sysctl(mib, idlevel + 2, (void *)label, labellen, NULL, 0));
}

int sysctlmif_info(int *id, size_t idlevel, void *info, size_t *infosize)
{
	int mib[CTL_MAXNAME + 2] = {CTL_SYSCTL, CTL_SYSCTL_OIDFMT};

	memcpy(mib + 2, id, idlevel*sizeof(int));
	return (sysctl(mib, idlevel + 2, info, infosize, NULL, 0));
}

int
sysctlmif_nextleaf(int *id, size_t idlevel, int *idnext, size_t *idnextlevel)
{
	int error, mib[CTL_MAXNAME + 2] = {CTL_SYSCTL, CTL_SYSCTL_NEXT};
	size_t tmp_nextlevel;

	memcpy(mib + 2, id, idlevel*sizeof(int));
	tmp_nextlevel = *idnextlevel * sizeof(int);
	error = sysctl(mib, idlevel + 2, idnext, &tmp_nextlevel, NULL, 0);
	if (error == 0)
		*idnextlevel = tmp_nextlevel / sizeof(int);

	return (error);
}

/* FreeBSD >= 13 */
#ifdef CTL_SYSCTL_NEXTNOSKIP
int
sysctlmif_nextnode(int *id, size_t idlevel, int *idnext, size_t *idnextlevel)
{
	int error, mib[CTL_MAXNAME + 2] = {CTL_SYSCTL, CTL_SYSCTL_NEXTNOSKIP};
	size_t tmp_nextlevel;

	memcpy(mib + 2, id, idlevel*sizeof(int));
	tmp_nextlevel = *idnextlevel * sizeof(int);
	error = sysctl(mib, idlevel + 2, idnext, &tmp_nextlevel, NULL, 0);
	if (error == 0)
		*idnextlevel = tmp_nextlevel / sizeof(int);

	return (error);
}
#else
int
sysctlmif_nextnode(int *id, size_t idlevel, int *idnext, size_t *idnextlevel)
{
	int error = 0;
	size_t i, minlevel;
	size_t tmp_nextlevel;
	/* if id = idnext */
	int previd[CTL_MAXNAME];
	size_t prevlevel = idlevel;

	memcpy(previd, id, idlevel * sizeof(int));

	tmp_nextlevel = *idnextlevel * sizeof(int);
	error = sysctlmif_nextleaf(id, idlevel, idnext, &tmp_nextlevel);
	if (error != 0) {
		return (error);
	}
	*idnextlevel = tmp_nextlevel;

	/*
	 * avoid:        id 5.6 -> next 5.6.4.8.2
	 * only 1 level: id 5.6 -> next 5.6.4
	 */
	if (*idnextlevel > prevlevel)
		*idnextlevel = prevlevel + 1;

	minlevel = *idnextlevel < prevlevel ? *idnextlevel : prevlevel;

	for (i = 0; i < minlevel; i++) {
		if (previd[i] != idnext[i]) {
			*idnextlevel = i+1;
			break;
		}
	}

	return (error);
}
#endif

struct sysctlmif_object *
sysctlmif_object(int *id, size_t idlevel, unsigned int flags)
{
	struct sysctlmif_object *obj = NULL;
	size_t size = 0;
	void *tmpinfo = NULL;

	if((obj = malloc(sizeof(struct sysctlmif_object))) == NULL)
		return (NULL);

	memset(obj, 0, sizeof(struct sysctlmif_object));

	/* id and idlevel */
	if((obj->id = malloc(idlevel * sizeof(int))) == NULL) {
		sysctlmif_freeobject(obj);
		return (NULL);
	}
	memcpy(obj->id, id, idlevel * sizeof(int));
	obj->idlevel = idlevel;

	if (flags & SYSCTLMIF_FNAME) {
		/* could be fake */
		if (SYSCTLMIF_NAMELEN(id, idlevel, &size) == 0) {
			if ((obj->name = malloc(size)) == NULL) {
				sysctlmif_freeobject(obj);
				return (NULL);
			}
			memset(obj->name, 0, size);
			if (sysctlmif_name(id, idlevel, obj->name,
			    &size) != 0) {
				obj->name = NULL;
			}
		}
	}

	if (flags & SYSCTLMIF_FDESC) {
		size = 0;
		/* no desc could be "\0" or NULL */
		if (SYSCTLMIF_DESCLEN(id, idlevel, &size) == 0) {
			if ((obj->desc = malloc(size)) == NULL) {
				sysctlmif_freeobject(obj);
				return (NULL);
			}
			memset(obj->desc, 0, size);
			if (sysctlmif_desc(id, idlevel, obj->desc,
			    &size) != 0) {
				obj->desc = NULL;
			}
		}
	}

	if (flags & SYSCTLMIF_FLABEL) {
		size = 0;
		if (SYSCTLMIF_LABELLEN(id, idlevel, &size) == 0) {
			if ((obj->label = malloc(size)) == NULL) {
				sysctlmif_freeobject(obj);
				return (NULL);
			}
			memset(obj->label, 0, size);
			if (sysctlmif_label(id, idlevel, obj->label,
			    &size) != 0) {
				obj->label = NULL;
			}
		}
	}

	if ((flags & SYSCTLMIF_FFLAGS) || (flags & SYSCTLMIF_FFMT) ||
	    (flags & SYSCTLMIF_FTYPE)) {
		size = 0;
		/* get info size because fmt is variable */
		if (sysctlmif_info(id, idlevel, NULL, &size) == 0) {
			tmpinfo = malloc(size);
			if (tmpinfo == NULL) {
				sysctlmif_freeobject(obj);
				return (NULL);
			}
			memset(tmpinfo, 0, size);

			if (sysctlmif_info(id, idlevel, tmpinfo, &size) < 0) {
				sysctlmif_freeobject(obj);
				free(tmpinfo);
				return (NULL);
			}

			if (flags & SYSCTLMIF_FFMT) {
				obj->fmt = strndup(SYSCTLMIF_INFOFMT(tmpinfo),
					size - sizeof(uint32_t));
				if (obj->fmt == NULL) {
					sysctlmif_freeobject(obj);
					return (NULL);
				}
			}

			if (flags & SYSCTLMIF_FFLAGS)
				obj->flags = SYSCTLMIF_INFOFLAGS(tmpinfo);

			if (flags & SYSCTLMIF_FTYPE)
				obj->type = SYSCTLMIF_INFOTYPE(tmpinfo);

			free(tmpinfo);
		}
	}

	return (obj);
}

void
sysctlmif_freeobject(struct sysctlmif_object *object)
{
	if (object == NULL)
		return;

	free(object->id);
	free(object->name);
	free(object->desc);
	free(object->label);
	free(object->fmt);
	free(object);
	object = NULL;
}

struct sysctlmif_list *
sysctlmif_filterlist(sysctlmif_filterfunc_t *filterfunc, unsigned int flags)
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME];
	size_t idlevel, idnextlevel;
	struct sysctlmif_list *list = NULL;
	struct sysctlmif_object *last, *new;

	if ((list = malloc(sizeof(struct sysctlmif_list))) == NULL)
		return (NULL);
	SLIST_INIT(list);

	id[0] = 0;
	idlevel = 1;
	for (;;) {
		if ((new = sysctlmif_object(id, idlevel, flags)) == NULL) {
			sysctlmif_freelist(list);
			return (NULL);
		}

		if ((filterfunc == NULL) || (filterfunc(new) == 0)) {
			if (SLIST_EMPTY(list)) {
				SLIST_INSERT_HEAD(list, new, object_link);
			} else {
				SLIST_INSERT_AFTER(last, new, object_link);
			}

			last = new;
		}

		idnextlevel = CTL_MAXNAME;
		if (sysctlmif_nextnode(id, idlevel, idnext, &idnextlevel) < 0) {
			break;
		}
		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;
	}

	return (list);
}

struct sysctlmif_list *
sysctlmif_grouplist(int *idstart, size_t idstartlen, unsigned int flags,
    unsigned int depth)
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME];
	size_t idlevel, idnextlevel;
	struct sysctlmif_list *list = NULL;
	struct sysctlmif_object *last, *new;
	size_t i;

	if((list = malloc(sizeof(struct sysctlmif_list))) == NULL)
		return (NULL);
	SLIST_INIT(list);

	memcpy(id, idstart, idstartlen * sizeof(int));
	idlevel = idstartlen;

	if ((new = sysctlmif_object(id, idlevel, flags)) == NULL) {
		free(list);
		return (NULL);
	}

	SLIST_INSERT_HEAD(list, new, object_link);

	last = new;

	for (;;) {
		idnextlevel = CTL_MAXNAME;
		if (sysctlmif_nextnode(id, idlevel, idnext, &idnextlevel) < 0)
			break;

		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;

		if (idlevel - idstartlen > depth)
			continue;

		if (idlevel < idstartlen)
			break;

		for (i = 0; i < idstartlen; i++)
			if (id[i] != idstart[i])
				return (list);

		new = sysctlmif_object(id, idlevel, flags);
		if (new == NULL) {
			sysctlmif_freelist(list);
			return (NULL);
		}
		SLIST_INSERT_AFTER(last, new, object_link);
		last = new;
	}

	return (list);
}

void sysctlmif_freelist(struct sysctlmif_list *list)
{
	struct sysctlmif_object *obj;

	if (list == NULL)
		return;

	while (!SLIST_EMPTY(list)) {
		obj = SLIST_FIRST(list);
		SLIST_REMOVE_HEAD(list, object_link);
		sysctlmif_freeobject(obj);
	}

	free(list);
	list = NULL;
}

struct sysctlmif_object *
sysctlmif_tree(int *id, size_t idlevel, unsigned int flags,
    unsigned int max_depth)
{
	struct sysctlmif_list *pathlist[CTL_MAXNAME + 1];
	struct sysctlmif_object *root, *child, *pathobj[CTL_MAXNAME + 1];
	int childid[CTL_MAXNAME], idnext[CTL_MAXNAME], end;
	size_t childidlevel, idnextlevel=CTL_MAXNAME;

	if ((root = sysctlmif_object(id, idlevel, flags)) == NULL) 
		return (NULL);

	if ((root->children = malloc(sizeof(struct sysctlmif_list))) == NULL) {
		sysctlmif_freeobject(root);
		return (NULL);
	}
	SLIST_INIT(root->children);

	if (max_depth < 1)
		return (root);

	pathlist[idlevel] = root->children;
	pathobj[idlevel] = root;
	if ((sysctlmif_nextnode(id, idlevel, idnext, &idnextlevel)) != 0)
		return root;

	while(idnextlevel > idlevel) {
		memcpy(childid, idnext, idnextlevel * sizeof(int));
		childidlevel = idnextlevel;

		idnextlevel=CTL_MAXNAME;
		end = sysctlmif_nextnode(childid, childidlevel, idnext,
		    &idnextlevel);

		if (idlevel + max_depth < childidlevel)
			continue;

		child = sysctlmif_object(childid, childidlevel,flags);
		if (child == NULL) {
			sysctlmif_freetree(root);
			return NULL;
		}
		child->children = malloc(sizeof(struct sysctlmif_list));
		if (child->children == NULL) {
			sysctlmif_freetree(root);
			return (NULL);
		}
		SLIST_INIT(child->children);

		if (SLIST_EMPTY(pathlist[childidlevel-1]))
			SLIST_INSERT_HEAD(pathlist[childidlevel-1], child,
			    object_link);
		else
			SLIST_INSERT_AFTER(pathobj[childidlevel], child,
			    object_link);

		pathlist[childidlevel] = child->children;
		pathobj[childidlevel] = child;

		if (end != 0)
			break;
	}

	return (root);
}

/* postorder visit */
void sysctlmif_freetree(struct sysctlmif_object *root)
{
	struct sysctlmif_object *child;

	if (root == NULL || root->children == NULL)
		return;

	while (!SLIST_EMPTY(root->children)) {
		child = SLIST_FIRST(root->children);
		SLIST_REMOVE_HEAD(root->children, object_link);
		sysctlmif_freetree(child);
	}
	sysctlmif_freeobject(root);
}

struct sysctlmif_list *sysctlmif_mib(unsigned int flags)
{
	struct sysctlmif_object *fake;
	struct sysctlmif_list *mib;
	int id[1] = {0};
	size_t idlevel=0;

	/*
	 *   id[0] = 0;
	 *   idlevel = 0;
	 *   root = sysctlmif_tree(id, idlevel, flags, depth);
	 *
	 *   despite the flags the root will have
	 *      root->id = NULL
	 *      root->idlevel = 0
	 *      root->name = NULL
	 *      root->desc = NULL;
	 *      root->label = NULL;
	 *      root->type = 0;
	 *      root->flags = 0;
	 *      root->fmt = NULL;
	 *      root->children;    `level 1` objects list with flags
	 */
	fake = sysctlmif_tree(id, idlevel, flags, SYSCTLMIF_MAXDEPTH);
	if (fake == NULL)
		return NULL;
	mib = fake->children;
	sysctlmif_freeobject(fake);

	return (mib);
}

void sysctlmif_freemib(struct sysctlmif_list *mib)
{
	struct sysctlmif_object *root;

	if (mib == NULL)
		return;

	while (!SLIST_EMPTY(mib)) {
		root = SLIST_FIRST(mib);
		SLIST_REMOVE_HEAD(mib, object_link);
		sysctlmif_freetree(root);
	}

	free(mib);
	mib = NULL;
}

